const url = require('url');
const PyszneAdapter = require('../adapters/pyszne');
const MinutaOsiemAdapter = require('../adapters/minutaosiem');

const OrderHelpers = (function () {
    let self;

    function OrderHelpers(robot) {
        self = this;
        self.robot = robot;
    }

    OrderHelpers.prototype.isAnyOrderOpen = function(){
        return self.robot.brain.get('site') != null;
    };

    OrderHelpers.prototype.isOrderCorrectlyOpened = function(){
        const choices = self.robot.brain.get('choices');
        return !self.isMapEmpty(choices);
    };

    OrderHelpers.prototype.getAdapter = function(site){
        const uri = url.parse(site, true);
        const host = uri.host;
        const address = uri.format();

        let result = null;
        switch(host){
            case "www.pyszne.pl":
                result = new PyszneAdapter(self.robot, address);
                break;
            case "minutaosiem.pl":
                result = new MinutaOsiemAdapter(self.robot, address);
                break;
        }
        return result;
    };

    OrderHelpers.prototype.isMapEmpty = function(obj) {
        for(const key in obj) {
            if(obj.hasOwnProperty(key)) return false;
        }
        return true;
    };

    OrderHelpers.prototype.findMatchingMeals = function(orderName){
        const choices = self.robot.brain.get('choices');
        const groups = Object.keys(choices);
        const result = {found: 0, results: []};
        for(const group of groups){
            for(const meal of choices[group]){
                if(isMealMatchingQuery(meal.name, orderName)){
                    result.found++;
                    result.results.push({
                        name: meal.name,
                        price: meal.price,
                        group: group
                    });
                }
            }
        }
        self.robot.logger.info(`Hubot searched orders for ${orderName} and found: ${JSON.stringify(result)}`);
        return result;
    };

    OrderHelpers.prototype.addOrder = function(user, order){
        let orders = self.robot.brain.get('orders') || {};
        let userOrders = orders[user] || [];
        userOrders.push(order);
        orders[user] = userOrders;
        self.robot.brain.set('orders', orders);
        self.robot.logger.info(`Orders changed for ${user} => ${JSON.stringify(order)}`);
        self.robot.logger.debug(`All orders:\n${JSON.stringify(orders)}`);
    };

    OrderHelpers.prototype.removeOrder = function(user, order){
        let orders = self.robot.brain.get('orders') || {};
        let userOrders = orders[user] || [];
        for(let userOrder of userOrders){
            self.robot.logger.debug(`Comparing ${JSON.stringify(userOrder)} with ${JSON.stringify(order)} in order to remove the latter...`);
            if(userOrder.name === order.name){
                self.robot.logger.info(`Removing ${JSON.stringify(userOrder)} from '${user}' user's list.`);
                const pos = userOrders.indexOf(userOrder);
                userOrders.splice(pos, 1);
                return true;
            }
        }
        return false;
    };

    OrderHelpers.prototype.getUserOrders = function(user){
        let orders = self.robot.brain.get('orders') || {};
        return orders[user] || [];
    };

    OrderHelpers.prototype.getAllOrders = function(){
        return self.robot.brain.get('orders') || {};
    };

    function isMealMatchingQuery(mealName, query){
        const r = query.toLowerCase().replace(/\s+/, '\\s+')
            .replace('ą', '[aą]').replace('ę', '[aę]').replace('ć', '[cć]').replace('ż', '[zż]')
            .replace('ź', '[zź]').replace('ó', '[oó]').replace('ł', '[lł]');
        const result = mealName.match(new RegExp(r, 'i'));
        self.robot.logger.debug(`Comparing '${mealName}' to /${r}/i: ${result}`);
        return result;
    }

    OrderHelpers.prototype.displayOrderInfo = function(order){
        return order.price != null ? `${order.name} za ${order.price} zł` : `${order.name}`;
    };

    return OrderHelpers;
}());

module.exports = OrderHelpers;