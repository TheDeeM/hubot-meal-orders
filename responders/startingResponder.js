const url = require('url');
const OrderHelpers = require('../helpers/orderHelpers');

const StartingResponder = (function () {
    let self;

    function StartingResponder(robot) {
        self = this;
        self.robot = robot;
        self.adapter = null;
        self.helpers = new OrderHelpers(robot);
    }

    StartingResponder.prototype.rememberOrder = function(message){
        const site = message.match[1];
        if(self.helpers.isAnyOrderOpen()){
            message.reply(`Hej, już zajmuję się zgłoszeniem ${self.robot.brain.get('site')}`);
        }
        else{
            self.adapter = self.helpers.getAdapter(site);
            if(self.adapter){
                self.robot.logger.info(`Using the ${self.adapter.adapterName()} adapter for the ${site} request.`);
                self.adapter.reloadChoices().then(() => {
                    if (self.helpers.isOrderCorrectlyOpened()){
                        self.robot.brain.set('site', site);
                        self.robot.logger.info(`Downloaded options: ${JSON.stringify(self.robot.brain.get('choices'))}`);
                        message.reply(`Rozpoczynam składanie zamówień!`);
                    }
                    else{
                        message.reply(`Niestety nie udało mi się załadować żadnego jedzenia ze strony ${site} :(`)
                    }
                });
            }
            else{
                message.reply(`Nie podałeś poprawnej strony młotku albo strona jest nieobsługiwana...`);
            }
        }
    };

    StartingResponder.prototype.displayMenu = function(message){
        processOrderFunctionality(message, () => {
            const choices = self.robot.brain.get('choices');
            const groups = Object.keys(choices);
            let answer = '';
            for(const group of groups){
                const meals = [];
                for(const meal of choices[group]){
                    meals.push(self.helpers.displayOrderInfo(meal));
                }
                answer += `Jeżeli masz ochotę na ${group}, to w menu dostępne są: ${meals.join(', ')}.\n`;
            }
            message.reply(answer);
        });
    };

    StartingResponder.prototype.addOrder = function(message){
        processOrderFunctionality(message, () => {
            const order = message.match[1].trim();
            processSearchingMatchingOrders(message, order, (foundOrder) => {
                const user = message.message.user.name;
                self.helpers.addOrder(user, foundOrder);
                message.reply(`OK, zapamiętam, że zamawiasz ${foundOrder.name} za ${foundOrder.price} zł`);
            })
        });
    };

    StartingResponder.prototype.removeOrder = function(message){
        processOrderFunctionality(message, () => {
            const order = message.match[1].trim();
            const user = message.message.user.name;

            processSearchingMatchingOrders(message, order, (foundOrder) => {
               if(self.helpers.removeOrder(user, foundOrder)){
                   message.reply(`OK, usunąłem ${foundOrder.name} z Twojej listy - dzięki temu zamówienie zmniejszyło się o ${foundOrder.price} zł`);
               }
               else{
                   message.reply(`Wygląda na to, że nie zamówiłeś jeszcze ${foundOrder.name} :)`)
               }
            })
        });
    };

    StartingResponder.prototype.displayUserOrders = function(message){
        processOrderFunctionality(message, () => {
            const user = message.message.user.name;
            const userOrders = self.helpers.getUserOrders(user);
            if(userOrders.length === 0){
                message.reply('Jeszcze nic nie zamówiłeś.')
            }
            else{
                let orders = [];
                let summary = parseFloat('0');
                for(let order of userOrders){
                    orders.push(self.helpers.displayOrderInfo(order));
                    summary += parseFloat(order.price);
                }
                message.reply(`Oto Twoje zamówienia:\n-${orders.join("\n-")}\nŁącznie masz do zapłaty ${summary.toFixed(2)} zł.`);
            }
        });
    };

    StartingResponder.prototype.displayAllOrders = function(message){
        processOrderFunctionality(message, () => {
            const allOrders = self.helpers.getAllOrders();
            const users = Object.keys(allOrders);
            if(users.length > 0){
                let orders = [];
                for(let user of users){
                    const userOrders = allOrders[user];
                    const userMeals = [];
                    for(let userOrder of userOrders){
                        userMeals.push(self.helpers.displayOrderInfo(userOrder));
                    }
                    orders.push(`Użytkownik ${user} zamówił ${userMeals.join(', ')}.`)
                }
                message.reply(`Lista zamówień:\n-${orders.join("\n-")}`);
            }
            else{
                message.reply('Nikt niczego jeszcze nie zamówił.');
            }
        });
    };

    function processOrderFunctionality(message, f){
        if(!self.helpers.isAnyOrderOpen()){
            message.reply(`Nic mi nie wiadomo o tym, że będziemy coś zamawiać...? Jeżeli chcesz to zmienić, poinformuj mnie o tym :)`);
        }
        else{
            f();
        }
    }

    function processSearchingMatchingOrders(message, order, f){
        const matchingOrders = self.helpers.findMatchingMeals(order);
        switch(matchingOrders.found){
            case 0:
                message.reply(`Niestety, wśród dostępnych zamówień nie znalazłem nic, co pasowałoby do ${order} :(`);
                break;
            case 1:
                f(matchingOrders.results[0]);
                break;
            default:
                message.reply(`Znalazłem więcej niż jedną pozycję z menu pasującą do ${order} (${JSON.stringify(matchingOrders.results)}) - nie wiem, o którą z nich chodzi :(.`);
        }
    }
    


    return StartingResponder;
}());

module.exports = StartingResponder;