// Description:
//   <description of the scripts functionality>
//
// Dependencies:
//   "<module name>": "<module version>"
//
// Configuration:
//   LIST_OF_ENV_VARS_TO_SET
//
// Commands:
//   hubot <trigger> - <what the respond trigger does>
//   <trigger> - <what the hear trigger does>
//
// Notes:
//   <optional notes required for the script>
//
// Author:
//   <github username of the original script author>
    
'use strict';

const StartingResponder = require('../responders/startingResponder');

module.exports = (robot) => {
  const startingResponder = new StartingResponder(robot);

  robot.respond(/b[eę]dziemy\s+zamawia[cć]\s+z[e]?\s+(.*)/i, startingResponder.rememberOrder);
  robot.respond(/(?:wy[sś]wietl\s+)?menu/i, startingResponder.displayMenu);
  robot.respond(/zam[oó]w\s+(.*)/i, startingResponder.addOrder);
  robot.respond(/usu[nń]\s+(.*)/i, startingResponder.removeOrder);
  robot.respond(/moje\s+zam[oó]wieni[ea]/i, startingResponder.displayUserOrders);
  robot.respond(/zam[oó]wienia/i, startingResponder.displayAllOrders);
};