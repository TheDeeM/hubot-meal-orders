const Helper = require('hubot-test-helper');

// helper loads a specific script if it's a file
const helper = new Helper('../scripts/orders.js');

const co     = require('co');
const expect = require('chai').expect;

class Chat {
    constructor(room, robotName){
        this.room = room;
        this.robotName = robotName || 'hubot';
    }

    user(userName){
        let self = this;
        return {
            sendsMessageToRobot: function(message){
                return co(function*() {
                    yield self.room.user.say(userName, `@${self.robotName} ${message}`);
                    yield new Promise((resolve) => {
                        setTimeout(function(){resolve();}, 100)
                    });
                }.bind(this));
            }
        }
    }
}

describe('making online meal orders', function() {
    beforeEach(function() {
        this.room = helper.createRoom();
    });
    afterEach(function() {
        this.room.destroy();
    });

    context('user', function() {
        beforeEach(function() {
            return co(function*() {
                yield this.room.user.say('alice', '@hubot hi');
                yield this.room.user.say('bob',   '@hubot hi');
            }.bind(this));
        });

        it('should reply to user', function() {
            const chat = new Chat(this.room);
            chat
                .user('alice').sendsMessageToRobot('hi');
                /*.then().robotShouldReply('hi')
                .user('bob').sendsMessage('hi')
                .then().robotShouldReply('hi');*/


            expect(this.room.messages).to.eql([
                ['alice', '@hubot hi'],
                ['hubot', '@alice hi'],
                ['bob',   '@hubot hi'],
                ['hubot', '@bob hi']
            ]);
        });
    });
});