const xpath = require('xpath');
const DOM = require('xmldom').DOMParser;
const parse5 = require('parse5');
const xmlser = require('xmlserializer');
const moment = require('moment');

const MinutaOsiemAdapter = (function () {
    let self;

    function MinutaOsiemAdapter(robot, address) {
        self = this;
        self.robot = robot;
        self.address = "http://minutaosiem.pl/menu/";
        moment.locale('pl');
    }

    MinutaOsiemAdapter.prototype.adapterName = function(){
        return "MinutaOsiem.pl";
    };

    MinutaOsiemAdapter.prototype.reloadChoices = function(){
        return new Promise((resolve, reject) => {
            self.robot.http(self.address).get()((err, res, body) => {
                if(err || res.responseStatus > 399){
                    reject();
                }
                else{
                    const choices = findChoicesFromHTML(body);
                    self.robot.brain.set('choices', choices);
                    resolve();
                }
            });
        });
    };

    function findChoicesFromHTML(html){
        const doc = getHtmlDocument(html);
        const select = xpath.useNamespaces({"x": "http://www.w3.org/1999/xhtml"});

        const table = findOrdersTable(doc, select);
        return loadOptionsFromTable(table, select, moment());
    }

    function getHtmlDocument(html){
        const document = parse5.parse(html);
        const xhtml = xmlser.serializeToString(document);
        return new DOM().parseFromString(xhtml);
    }

    function findOrdersTable(doc, select) {
        const query = '//x:div[@class="table-box"]//x:table';
        return select(query, doc);
    }

    function loadOptionsFromTable(table, select, date){
        const doc = getHtmlDocument(table.toString());
        const rows = select('//x:table//x:tr', doc);
        const dates = extractValuesFromTableRow(rows[1], select);
        const standardMenu = extractValuesFromTableRow(rows[2], select);
        const vegeMenu = extractValuesFromTableRow(rows[3], select);
        const dayIndex = findIndexForDate(dates, select, date);
        let result = {};

        self.robot.logger.info(`Hubot found meals in the standard menu: ${JSON.stringify(standardMenu)}`);
        self.robot.logger.info(`Hubot found meals in the vege menu: ${JSON.stringify(vegeMenu)}`);
        self.robot.logger.info(`Hubot menu for dates: ${JSON.stringify(dates)}. Result will be displayed for ${date} (${dayIndex} position).`);

        result['standard'] = [standardMenu[dayIndex]];
        result['vege'] = [vegeMenu[dayIndex]];

        return result;
    }

    function extractValuesFromTableRow(row, select){
        const doc = getHtmlDocument(`<table>${row.toString()}</table>`);
        const cells = select('//x:td', doc);
        let result = [];
        for(let cell of cells){
            const cellDoc = getHtmlDocument(`<table><tr>${cell.toString()}</tr></table>`);
            const value = select('//x:td//text()', cellDoc);
            const dishName = value.toString().replace(/,\s+/, ' ').trim().replace(/[,\s0-9*]+$/, '');
            result.push({
                name: dishName
            });
        }
        return result;
    }

    function findIndexForDate(dates, select, date){
        const search = moment(date).format('dddd DD.MM').toString().toLowerCase();

        for(let i = 1; i < dates.length; i++){
            if(dates[i].name.toString().toLowerCase() === search){
                return i;
            }
        }
        return 1;
    }

    return MinutaOsiemAdapter;
}());

module.exports = MinutaOsiemAdapter;