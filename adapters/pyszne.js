const xpath = require('xpath');
const DOM = require('xmldom').DOMParser;
const parse5 = require('parse5');
const xmlser = require('xmlserializer');

const PyszneAdapter = (function () {
    let self;

    function PyszneAdapter(robot, address) {
        self = this;
        self.robot = robot;
        self.address = address;
    }

    PyszneAdapter.prototype.adapterName = function(){
        return "Pyszne.pl";
    };

    PyszneAdapter.prototype.reloadChoices = function(){
        return new Promise((resolve, reject) => {
            self.robot.http(self.address).get()((err, res, body) => {
                if(err || res.responseStatus > 399){
                    reject();
                }
                else{
                    const choices = findChoicesFromHTML(body);
                    self.robot.brain.set('choices', choices);
                    resolve();
                }
            });
        });
    };

    function findChoicesFromHTML(html){
        const select = xpath.useNamespaces({"x": "http://www.w3.org/1999/xhtml"});
        const groups = findGroupsForMeals(html, select) || [];

        let result = {};
        self.robot.logger.info(`Hubot found ${groups.length} groups on the site ${self.address}`);
        for(let group of groups){
            const groupDocument = getHtmlDocument(group.toString());
            const groupName = findGroupName(groupDocument, select);
            const meals = findMealsForGroup(groupDocument, select);
            self.robot.logger.info(`Hubot found ${meals.length} meals for group '${groupName}' on the site ${self.address}`);
            result[groupName.toLowerCase()] = meals;
        }

        return result;
    }

    function findGroupsForMeals(html, select){
        const doc = getHtmlDocument(html);
        return select('//x:div[@class="menucard"]/x:div[@class="menu-meals-group"]', doc);
    }

    function findMealsForGroup(group, select){
        const nodes = select('//x:div[@class="meal"]', group);

        let result = [];
        for(const node of nodes){
            const nodeDocument = getHtmlDocument(node.toString());
            const name = findNameOfMeal(nodeDocument, select);
            const price = findPriceOfMeal(nodeDocument, select);
            result.push({name, price})
        }
        return result;
    }

    function getHtmlDocument(html){
        const document = parse5.parse(html);
        const xhtml = xmlser.serializeToString(document);
        return new DOM().parseFromString(xhtml);
    }

    function findGroupName(group, select){
        const query = '//x:div[@class="menu-meals-group"]/x:div[@class="menu-meals-group-category"]/x:div[@class="menu-category-head"]/x:div/x:span/text()';
        return select(query, group).toString().trim();
    }

    function findNameOfMeal(node, select) {
        return select('//x:div[@class="meal"]/x:div[@class="meal-wrapper"]//x:span[@class="meal-name"]/text()', node).toString().trim();
    }

    function findPriceOfMeal(node, select) {
        const priceString = select('//x:div[@class="meal"]/x:div[@class="meal-add-btn-wrapper"]//x:span/text()', node).toString().trim();
        const onlyPrice = priceString.match(/[\d\s.,]+/)[0].trim().replace(',', '.');
        return parseFloat(onlyPrice).toFixed(2);
    }

    function findOptionsForMeal(node, select){
        let result = {};
        const sideDishes = select('//x:div[@class="meal"]/x:div[@class="meal-wrapper"]//x:span[@class="meal-name"]/text()', node).toString().trim();
        // TODO: The sidedishes are not inside the HTML from the start, you have to click the name first
        return result;
    }

    return PyszneAdapter;
}());

module.exports = PyszneAdapter;